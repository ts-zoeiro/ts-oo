import Carro from "./Carro";
import Moto from "./Moto";
import Concessionaria from "./Concessionaria";

let carro = new Carro("ipanema", 4);
let moto = new Moto();

carro.acelerar();
moto.acelerar();


console.log(carro, moto);

let concessionaria = new Concessionaria("lala", []);
console.log(concessionaria.fornecerHorario());