export default interface I_DAO {
    nomeTabela: string;

    inserir(object: any) : boolean;
    atualizar(object: any) : boolean;
    remover(id: number) : any;
    selecionar(id: number): any;
    selecionarTudo(): Array<any>;
}