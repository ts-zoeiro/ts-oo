export default interface I_GenericDAO<T> {
    nomeTabela: string;

    inserir(object: T) : boolean;
    atualizar(object: T) : boolean;
    remover(id: number) : T;
    selecionar(id: number): T;
    selecionarTudo(): Array<T>;
}