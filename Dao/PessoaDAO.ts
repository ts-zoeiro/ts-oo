import I_DAO from "./I_DAO";
import Pessoa from "../Pessoa";

export default class PesssoaDAO implements I_DAO {
    nomeTabela: string = "tb_concessionaria";
    
    inserir(object: Pessoa) : boolean {
        console.log("logica de insert");
        return true;
    }
    atualizar(object: Pessoa) : boolean {
        console.log("logica de update");
        return true;
    }
    remover(id: number) : Pessoa {
        console.log("logica de remover");
        return new Pessoa("", "");
    }
    selecionar(id: number) : Pessoa {
        console.log("logica selecionar");
        return new Pessoa("", []);
    }
    
    selecionarTudo(): Array<Pessoa> {
        console.log("logica get all")
        return [new Pessoa("", "")];
    }
    
    
    
}