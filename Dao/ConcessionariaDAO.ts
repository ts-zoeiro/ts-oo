import I_DAO from "./I_DAO";
import Concessionaria from "../Concessionaria";

export default class ConcessionariaDao implements I_DAO {
    nomeTabela: string = "tb_concessionaria";
    
    inserir(object: Concessionaria) : boolean {
        console.log("logica de insert");
        return true;
    }
    atualizar(object: Concessionaria) : boolean {
        console.log("logica de update");
        return true;
    }
    remover(id: number) : Concessionaria {
        console.log("logica de remover");
        return new Concessionaria("", []);
    }
    selecionar(id: number) : Concessionaria {
        console.log("logica selecionar");
        return new Concessionaria("", []);
    }
    
    selecionarTudo(): Array<Concessionaria> {
        console.log("logica get all")
        return [new Concessionaria("", [])];
    }
    
    
    
}