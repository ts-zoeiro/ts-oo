import I_GenericDAO from "./I_GenericDAO";
export default class GenericDAO<T>  implements I_GenericDAO<T> {
    nomeTabela: string = "";
    
    inserir(object: T) : boolean {
        console.log("logica de insert");
        return true;
    }
    atualizar(object: T) : boolean {
        console.log("logica de update");
        return true;
    }
    remover(id: number) : T {
        console.log("logica de remover");
        return  Object();
    }
    selecionar(id: number) : T {
        console.log("logica selecionar");
        return Object();
    }
    
    selecionarTudo(): Array<T> {
        console.log("logica get all")
        return [Object()];
    }
    
}