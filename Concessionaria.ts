import Carro from "./Carro";
import IConcessionaria from "./Iconcessionara";

export default class Concessionaria implements IConcessionaria {
    constructor(private endereco: string, private listaDeCarros: Array<Carro>) {

    }

    public fornecerHorario(): string {
        return "Segunda a sexta das 08:00 as 18:00";
    }

    public fornecerEndereco(): string {
        return this.endereco;
    }
    public mostrarListaDeCarros(): Array<Carro> {
        return this.listaDeCarros;
    }
}

