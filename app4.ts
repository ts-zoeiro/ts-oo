import GenericDAO from "./Dao/GenericDAO";
import Pessoa from "./Pessoa";
import Concessionaria from "./Concessionaria";


let concessionaria: Concessionaria = new Concessionaria("av dos burg", []);
let pessoa: Pessoa = new Pessoa("alipio", "monza");

let pessoaDAO: GenericDAO<Pessoa> = new GenericDAO<Pessoa>();
let concessionariaDAO: GenericDAO<Concessionaria> = new GenericDAO<Concessionaria>();


pessoaDAO.atualizar(pessoa);
concessionariaDAO.remover(0);